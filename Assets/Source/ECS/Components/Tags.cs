using Unity.Entities;

namespace Source.ECS.Components
{
    //Надо отсортировать в алфавитном порядке...

    public struct GameModeTag : IComponentData
    {
    }

    public struct CellTag : IComponentData
    {
    }

    public struct SubRegionTag : IComponentData
    {
    }

    public struct FractionalRegionTag : IComponentData
    {
    }

    public struct RegionTag : IComponentData
    {
    }

    public struct ResourceTag : IComponentData
    {
    }

    public struct BuildingTag : IComponentData
    {
    }

    public struct CountryTag : IComponentData
    {
    }

    public struct WorkInProgressTag : IComponentData
    {
    }

    public struct CellModifierTag : IComponentData
    {
    } 
  
    public struct MarketTag : IComponentData
    {
    }
    

    /*
    
    

    public struct ExploringCellTaskTag : IComponentData
    {
    }
     */
}