using Source.ECS.Utilities;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;

namespace Source.ECS.Components
{
    public struct RegionRef : IComponentData
    {
        public Entity Value;
    }
    
    public struct Owner : IComponentData
    {
        public Entity Value;
    }
    
    public struct Capital : IComponentData
    {
        public Entity Value;
    }
    
    [InternalBufferCapacity(32)]
    public struct CellBuffer : IBufferElementData
    {
        public Entity Value;
    }
    
    [InternalBufferCapacity(6)]
    public struct CellNeighbors : IBufferElementData
    {
        public Entity Value;
    }
    
    [InternalBufferCapacity(16)]
    public struct RegionBuffer : IBufferElementData
    {
        public Entity Value;
    }
    
    [InternalBufferCapacity(16)]
    public struct BuildingBuffer : IBufferElementData
    {
        public Entity Value;
    }
    
    [InternalBufferCapacity(8)]
    public struct InputBuffer : IBufferElementData
    {
        public Entity Entity;
        public float Size;
    }
    
    [InternalBufferCapacity(8)]
    public struct OutputBuffer : IBufferElementData
    {
        public Entity Entity;
        public float Size;
    }

    [InternalBufferCapacity(16)]
    public struct ResourceBuffer : IBufferElementData
    {
        public Entity Entity;
        public float Size;
    }
    
    [InternalBufferCapacity(16)]
    public struct CellModifierBuffer : IBufferElementData
    {
        public Entity Modifier;
        public Entity Building;
    }
    
    public struct NameComponent : IComponentData
    {
        public FixedString128Bytes Value;
    }

    [MaterialProperty("_ColorComponent", MaterialPropertyFormat.Float4)]
    public struct ColorComponent : IComponentData
    {
        public float4 Value;
    }

    public struct CubeCoordinate : IComponentData
    {
        public int3 Value;
    }

    public struct TurnCounter : IComponentData
    {
        public int Value;
    }

    public struct IsNextTurn : IComponentData
    {
        public bool Value;
    }

    public struct PlayerData : IComponentData
    {
        public bool UpdateUI;
        public Entity TouchedCell;
        public Entity OwnCountry;
        public bool UpdateMap;
        public MapType CurrentMap;
    }

    public struct BuildData : IComponentData
    {
        public int Time;
        public Entity Resource;
        public float ResourceCount;
        public bool NeedCellModifier;
        public Entity ModifierEntity;
    }

    public struct ModifierRef : IComponentData
    {
        public Entity CellEntity;
        public int Index;
    }

    public struct MarketRef : IComponentData
    {
        public Entity Value;
    }
    
    [InternalBufferCapacity(16)]
    public struct MarketResourceBuffer : IBufferElementData
    {
        public Entity ResourceEntity;
        public float Size;
        public float Price;
    }

    public struct Money : IComponentData
    {
        public float Value;
    }
    
    /*
     * 
    
    public struct BuildBuildingTaskRulesData : IComponentData
    {
        public int Time;
        public Entity Resource;
        public float ResourceCount;
    }
    
    public struct BuildCityTaskRules : IComponentData
    {
        public int Time;
        public FixedString128Bytes ResourceName;
        public float ResourceCount;
        public FixedString128Bytes BuildingName1;
        public FixedString128Bytes BuildingName2;
    }
    
    
    public struct CityPopulation : IComponentData
    {
        public int FreePeople;
        public int TotalPeople;
    }
    
    public struct MaxPopulation : IComponentData
    {
        public int Value;
    }
    
    
    public struct CityRulesData : IComponentData
    {
        public FixedList32Bytes<int> BuildingCount;
        public FixedList32Bytes<int> MaxPeople;
    }
    
    
    public struct ExploringCellData : IComponentData
    {
        public int Time;
        public Entity Resource;
        public float ResourceCount;
    }

    

     */
}