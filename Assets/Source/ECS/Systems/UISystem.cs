using System;
using System.Globalization;
using Source.ECS.Components;
using Source.ECS.Components.AuthoringComponents;
using TMPro;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Source.ECS.Systems
{
    [AlwaysUpdateSystem]
    [DisableAutoCreation]
    [UpdateAfter(typeof(NextTurnSystem))]
    [UpdateAfter(typeof(InputSystem))]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public partial class UISystem : SystemBase
    {
        private Entity _player;
        private Entity _gameMode;

        //RightWindow
        private TMP_Text _cellText;
        private TMP_Text _turnText;

        //MarketWindow
        private TMP_Text _marketText;
        private TMP_Text _moneyText;

        //BuildMenu
        private TMP_Text _regionText;

        protected override void OnCreate()
        {
            _gameMode = BaseSystems.GetSingleEntity<GameModeTag>(EntityManager);
            _player = BaseSystems.GetSingleEntity<PlayerTag>(EntityManager);

            //RightWindow
            {
                _cellText = GameObject.Find("CellText").GetComponent<TMP_Text>();

                _turnText = GameObject.Find("TurnText").GetComponent<TMP_Text>();

                {
                    var button = GameObject.Find("TurnButton").GetComponent<Button>();
                    button.onClick.AddListener(() => { SetComponent(_gameMode, new IsNextTurn { Value = true }); });
                }

                var settleButton = GameObject.Find("SettleButton").GetComponent<Button>();
                settleButton.onClick.AddListener(() =>
                {
                    var playerData = GetComponent<PlayerData>(_player);
                    var countryEntity = GetComponent<PlayerData>(_player).OwnCountry;
                    var regionEntity = GetComponent<Capital>(countryEntity).Value;
                    BaseSystems.AddCell(
                        playerData.TouchedCell,
                        regionEntity, 
                        EntityManager
                        );
                });
            }

            //MarketWindow
            {
                _marketText = GameObject.Find("MarketText").GetComponent<TMP_Text>();
                _moneyText = GameObject.Find("MoneyText").GetComponent<TMP_Text>();

                var resourceInMarketDropdown = GameObject.Find("ResourceInMarketDropdown").GetComponent<TMP_Dropdown>();
                {
                    var list = resourceInMarketDropdown.options;
                    list.Clear();
                    var query = GetEntityQuery(ComponentType.ReadOnly<ResourceTag>());
                    var entities = query.ToEntityArray(Allocator.Persistent);
                    foreach (var entity in entities)
                    {
                        var name = GetComponent<NameComponent>(entity);
                        list.Add(new TMP_Dropdown.OptionData(name.Value.ToString()));
                    }

                    resourceInMarketDropdown.value = 1;
                    resourceInMarketDropdown.value = 0;
                    entities.Dispose();
                }
                
                var countryEntity = GetComponent<PlayerData>(_player).OwnCountry;
                var sizeText = GameObject.Find("SizeText").GetComponent<TMP_InputField>();
                sizeText.text = "0";
                {
                    var btn = GameObject.Find("BuyButton").GetComponent<Button>();
                    btn.onClick.AddListener(() =>
                    {
                        BaseSystems.TryBuy(resourceInMarketDropdown.options[resourceInMarketDropdown.value].text,
                            Convert.ToInt32(sizeText.text), countryEntity, EntityManager);
                        UpdateMarketWindow();
                    });
                }
                {
                    var btn = GameObject.Find("SaleButton").GetComponent<Button>();
                    btn.onClick.AddListener(() =>
                    {
                        BaseSystems.TrySale(resourceInMarketDropdown.options[resourceInMarketDropdown.value].text,
                            Convert.ToInt32(sizeText.text), countryEntity, EntityManager);
                        UpdateMarketWindow();
                    });
                }
            }
            
            //BuildMenu
            {
                var buildingDropdown = GameObject.Find("BuildingDropdown").GetComponent<TMP_Dropdown>();
                {
                    var list = buildingDropdown.options;
                    list.Clear();
                    var query = GetEntityQuery(ComponentType.ReadOnly<BuildingTag>(), ComponentType.ReadOnly<Prefab>());
                    var entities = query.ToEntityArray(Allocator.Persistent);
                    foreach (var entity in entities)
                    {
                        var name = GetComponent<NameComponent>(entity);
                        list.Add(new TMP_Dropdown.OptionData(name.Value.ToString()));
                    }

                    buildingDropdown.value = 1;
                    buildingDropdown.value = 0;
                    entities.Dispose();
                }
                
                _regionText = GameObject.Find("RegionText").GetComponent<TMP_Text>();
                
                {
                    var btn = GameObject.Find("BuildButton").GetComponent<Button>();
                    var countryEntity = GetComponent<PlayerData>(_player).OwnCountry;
                    var regionEntity = GetComponent<Capital>(countryEntity).Value;
                    btn.onClick.AddListener(() =>
                    {
                        BaseSystems.AddBuilding(regionEntity, buildingDropdown.options[buildingDropdown.value].text,  EntityManager);
                        UpdateBuildWindow();
                    });
                }
            }

            //UpMenu
            {
                {
                    var btn = GameObject.Find("ExitButton").GetComponent<Button>();
                    btn.onClick.AddListener(Application.Quit);
                }

                {
                    var marketWindow = GameObject.Find("MarketWindow");
                    var btn = GameObject.Find("MarketButton").GetComponent<Button>();
                    btn.onClick.AddListener(() =>
                    {
                        marketWindow.SetActive(!marketWindow.activeSelf);
                        UpdateMarketWindow();
                    });
                    marketWindow.SetActive(false);
                }

                {
                    var buildWindow = GameObject.Find("BuildWindow");
                    var btn = GameObject.Find("BuildingButton").GetComponent<Button>();
                    btn.onClick.AddListener(() =>
                    {
                        buildWindow.SetActive(! buildWindow.activeSelf);
                        UpdateBuildWindow();
                    });
                    buildWindow.SetActive(false);
                }
            }

            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var playerData = GetComponent<PlayerData>(_player);
            if(!playerData.UpdateUI) return;
            
            UpdateCellInfo(playerData.TouchedCell);
            UpdateTurnText();
            EndUpdate(playerData);
        }
        
        private void UpdateCellInfo(Entity cell)
        {
            if (!EntityManager.Exists(cell)) return;
            
            _cellText.text = "Current cell \n";
            _cellText.text += $"Cube coordinate: {GetComponent<CubeCoordinate>(cell).Value}\n";
            _cellText.text += "\n";

            var modifierBuffer = GetBuffer<CellModifierBuffer>(cell);
            _cellText.text += "Modifiers: \n";
            for (var i = 0; i < modifierBuffer.Length; i++)
            {
                _cellText.text += $"\t{GetComponent<NameComponent>(modifierBuffer[i].Modifier).Value.ToString()}\n";
            }

            _cellText.text += "\n";
        }

        private void UpdateTurnText()
        {
            _turnText.text = $"End Turn\n Turn {GetComponent<TurnCounter>(_gameMode).Value}";
        }

        private void UpdateMarketWindow()
        {
            var countryEntity = GetComponent<PlayerData>(_player).OwnCountry;
            _moneyText.text = $"Money: {GetComponent<Money>(countryEntity).Value}";

            var regionEntity = GetComponent<RegionRef>(GetComponent<Capital>(countryEntity).Value).Value;
            var marketEntity = GetComponent<MarketRef>(regionEntity).Value;

            var marketResourceBuffer = GetBuffer<MarketResourceBuffer>(marketEntity);
            _marketText.text = "Resource\tSize\tPrice\n";
            for (var i = 0; i < marketResourceBuffer.Length; i++)
            {
                var name = GetComponent<NameComponent>(marketResourceBuffer[i].ResourceEntity).Value;
                _marketText.text += $"{name}\t";
                if (name.Length < 6) _marketText.text += "\t";
                _marketText.text += $"{marketResourceBuffer[i].Size}\t";
                _marketText.text += $"{marketResourceBuffer[i].Price}\n";
            }
        }

        private void UpdateBuildWindow()
        {
            var countryEntity = GetComponent<PlayerData>(_player).OwnCountry;
            var regionEntity = GetComponent<Capital>(countryEntity).Value;
            if(!EntityManager.Exists(regionEntity)) return;
            var resources = GetBuffer<ResourceBuffer>(regionEntity);
            var buildings = GetBuffer<BuildingBuffer>(regionEntity);
            
            _regionText.text = "Current region \n";
            _regionText.text += $"Name: {GetComponent<NameComponent>(regionEntity).Value.ToString()}\n";
            
            _regionText.text += "Resources: \n";
            foreach (var resource in resources)
            {
                _regionText.text += $"\t{GetComponent<NameComponent>(resource.Entity).Value.ToString()}: {resource.Size}\n";
            }
            _regionText.text += "Buildings: \n";
            foreach (var building in buildings)
            {
                _regionText.text += $"\t{GetComponent<NameComponent>(building.Value).Value.ToString()}\n";
            }
            _regionText.text += "\n";
        }
        
        private void EndUpdate(PlayerData playerData)
        {
            playerData.UpdateUI = false;
            SetComponent(_player, playerData);
        }
        
        /*
        private void CheckButton(Entity cell)
        {
            _exploreBtn.interactable = false; 
            _buildBuildingBtn.interactable = false;
            if (cell == Entity.Null) return;
            
            var country = GetComponent<CountryRef>(_player);
            if(country.Value == Entity.Null) return;
            
            _exploreBtn.interactable = CheckPossibilityAddExploringCellTask(cell, country.Value, EntityManager);
            
            var city = GetComponent<CityRef>(cell);
            if(city.Value == Entity.Null) return;
            var building = FindByName<BuildingRulesTag>(_buildingList.options[_buildingList.value].text, EntityManager);
            _buildBuildingBtn.interactable = CheckPossibilityAddBuildBuildingTask(city.Value, building, country.Value, EntityManager);
        }
        
        
        */
    }
}