using Source.ECS.Components;
using Source.ECS.Components.AuthoringComponents;
using Unity.Collections;
using Unity.Entities;
using static Source.ECS.Systems.BaseSystems;
using static Source.ECS.Utilities.EntitiesList;

namespace Source.ECS.Systems
{
    [DisableAutoCreation]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(MapSystem))]
    public partial class NextTurnSystem : SystemBase
    {
        private Entity _gameMode;

        protected override void OnCreate()
        {
            _gameMode = GetSingleEntity<GameModeTag>(EntityManager);

            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            if (!GetComponent<IsNextTurn>(_gameMode).Value)
                return;

            WorkBuilding();
            IncrementTurnCounter();
            
            BuildBuildingCheck();
            
            UpdatePlayerUI();
            UpdateMap();
            
            EndTurn();
        }

        private void WorkBuilding()
        {
            var inputBuffers = GetBufferFromEntity<InputBuffer>();
            var outputBuffers = GetBufferFromEntity<OutputBuffer>();
            var resourceBuffers = GetBufferFromEntity<ResourceBuffer>();

            Entities.WithAll<BuildingTag>().WithNone<WorkInProgressTag>().ForEach(
                (Entity entity,  ref Owner owner, in ModifierRef modifierRef) =>
                {
                    if (!HasComponent<CellTag>(modifierRef.CellEntity))
                        return;
                    
                    
                    var inputBuffer = inputBuffers[entity];
                    var outputBuffer = outputBuffers[entity];
                    var resourceBuffer = resourceBuffers[owner.Value];

                    var inputResourceIndex = new NativeArray<int>(inputBuffer.Length, Allocator.Temp);

                    for (var i = 0; i < inputBuffer.Length; i++)
                    {
                        var inputResource = inputBuffer[i];
                        var index = FindOrCreateInBuffer(inputResource.Entity, resourceBuffer);
                        if (resourceBuffer[index].Size - inputResource.Size > 0.01)
                        {
                            inputResourceIndex[i] = index;
                        }
                        else
                        {
                            inputResourceIndex.Dispose();
                            return;
                        }
                    }

                    for (var i = 0; i < inputBuffer.Length; i++)
                    {
                        var inputResource = inputBuffer[i];
                        var temp = resourceBuffer[inputResourceIndex[i]];
                        temp.Size -= inputResource.Size;
                        resourceBuffer[inputResourceIndex[i]] = temp;
                    }

                    inputResourceIndex.Dispose();

                    for (var i = 0; i < outputBuffer.Length; i++)
                    {
                        var outputResource = outputBuffer[i];
                        var index = FindOrCreateInBuffer(outputResource.Entity, resourceBuffer);
                        var temp = resourceBuffer[index];
                        temp.Size += outputResource.Size;
                        resourceBuffer[index] = temp;
                    }
                }
            ).WithoutBurst().Run();
        }

        private void IncrementTurnCounter()
        {
            Entities.ForEach((ref TurnCounter turnCounter) => { turnCounter.Value++; }).ScheduleParallel();
        }

        private void BuildBuildingCheck()
        {
            var system = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
            var ecb = system.CreateCommandBuffer().AsParallelWriter();

            var buildBuildingType = new ComponentTypes(BuildBuildingTypes);

            Entities.WithAll<BuildingTag, WorkInProgressTag>().ForEach(
                (Entity entity, int entityInQueryIndex, in TurnCounter turnCounter, in BuildData buildData) =>
                {
                    if (turnCounter.Value >= buildData.Time)
                    {
                        ecb.RemoveComponent(entityInQueryIndex, entity, buildBuildingType);
                    }
                }
            ).ScheduleParallel();

            system.AddJobHandleForProducer(Dependency);
        }

        private void UpdatePlayerUI()
        {
            Entities.WithAll<PlayerTag>().ForEach((ref PlayerData playerData) => { playerData.UpdateUI = true; })
                .ScheduleParallel();
        }

        private void UpdateMap()
        {
            Entities.WithAll<PlayerTag>().ForEach((ref PlayerData playerData) => { playerData.UpdateMap = true; })
                .ScheduleParallel();
        }
        
        private void EndTurn()
        {
            SetComponent(_gameMode, new IsNextTurn { Value = false });
        }

        /*
        private void ExplorerTaskCheck()
        {
          var entityCommandBuffer =
              World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>().CreateCommandBuffer();

          Entities.WithAll<ExploringCellTaskTag>().ForEach(
              (Entity entity, in TurnCounter turnCounter, in CountryRef countryRef, in CellRef cellRef,
                  in ExploringCellData data) =>
              {
                  if (turnCounter.Value >= data.Time)
                  {
                      var cellCountryExplored = GetBuffer<CellCountryExplored>(cellRef.Value);
                      cellCountryExplored.Add(new CellCountryExplored { Value = countryRef.Value });
                      entityCommandBuffer.DestroyEntity(entity);
                  }
              }
          ).WithoutBurst().Run();
          
        }
        */
    }
}