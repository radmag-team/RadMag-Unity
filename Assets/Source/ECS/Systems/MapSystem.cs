using System;
using Source.ECS.Components;
using Source.ECS.Components.AuthoringComponents;
using Source.ECS.Utilities;
using Unity.Entities;
using Unity.Assertions;
using Unity.Mathematics;
using static Source.ECS.Systems.BaseSystems;

namespace Source.ECS.Systems
{
    [AlwaysUpdateSystem]
    [DisableAutoCreation]
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    public partial class MapSystem : SystemBase
    {
        private Entity _player;

        protected override void OnCreate()
        {
            _player = GetSingleEntity<PlayerTag>(EntityManager);
            Assert.IsTrue(_player != Entity.Null);

            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            var playerData = GetComponent<PlayerData>(_player);
            if (!playerData.UpdateMap) return;

            switch (playerData.CurrentMap)
            {
                case MapType.PoliticalMap:
                    SetPoliticalMap();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            playerData.UpdateMap = false;
            SetComponent(_player, playerData);
        }

        private void SetPoliticalMap()
        {
            var system = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();
            var ecb = system.CreateCommandBuffer().AsParallelWriter();

            Entities.WithAll<CellTag>().ForEach(
                (Entity entity, int entityInQueryIndex, in RegionRef regionRef) =>
                {
                    var countryEntity = GetComponent<Owner>(regionRef.Value).Value;
                    var countryColor = new ColorComponent();
                    if (HasComponent<CountryTag>(countryEntity))
                    {
                        countryColor = GetComponent<ColorComponent>(countryEntity);
                    }
                    else
                    {
                        countryColor.Value = new float4(0.8f);
                    }

                    ecb.SetComponent(entityInQueryIndex, entity, countryColor);
                }
            ).ScheduleParallel();
            
            
            system.AddJobHandleForProducer(Dependency);
        }
    }
}