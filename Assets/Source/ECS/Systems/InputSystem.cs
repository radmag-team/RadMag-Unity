using Source.ECS.Components;
using Source.ECS.Components.AuthoringComponents;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using Unity.Assertions;
using RaycastHit = Unity.Physics.RaycastHit;
using static Source.ECS.Systems.BaseSystems;
using UnityEngine.EventSystems;

namespace Source.ECS.Systems
{
    [AlwaysUpdateSystem]
    [DisableAutoCreation]
    public partial class InputSystem : SystemBase
    {
        private BuildPhysicsWorld _buildPhysicsWorld;
        private CollisionWorld _collisionWorld;
        private Entity _player;

        protected override void OnCreate()
        {
            _buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();

            _player = GetSingleEntity<PlayerTag>(EntityManager);
            Assert.IsTrue(_player != Entity.Null);
            
            base.OnCreate();
        }

        protected override void OnUpdate()
        {
            MovePlayer();

            if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
            {
                if (!GetComponent<PlayerData>(_player).UpdateUI)
                {
                    TouchHex();
                }
            }
        }

        private void MovePlayer()
        {
            Vector3 move = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
            {
                move += Vector3.up;
            }

            if (Input.GetKey(KeyCode.S))
            {
                move += Vector3.down;
            }

            if (Input.GetKey(KeyCode.D))
            {
                move += Vector3.right;
            }

            if (Input.GetKey(KeyCode.A))
            {
                move += Vector3.left;
            }

            if (Input.GetKey(KeyCode.LeftControl))
            {
                move += Vector3.back;
            }

            if (Input.GetKey(KeyCode.LeftShift))
            {
                move += Vector3.forward;
            }
            
            float3 newPosition = new float3(move.x, move.y, move.z);
            newPosition *= 0.1f;

            var translation = GetComponent<Translation>(_player);
            translation.Value += newPosition;
            SetComponent(_player, translation);
        }

        private void TouchHex()
        {
            _collisionWorld = _buildPhysicsWorld.PhysicsWorld.CollisionWorld;

            var mainCamera = World.EntityManager.GetComponentObject<Camera>(_player);
            var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            var rayStart = ray.origin;
            var rayEnd = ray.GetPoint(100f);

            if (Raycast(rayStart, rayEnd, out var raycastHit))
            {
                var hitEntity = _buildPhysicsWorld.PhysicsWorld.Bodies[raycastHit.RigidBodyIndex].Entity;
                if (EntityManager.HasComponent<CubeCoordinate>(hitEntity))
                {
                    var guiUpdateInfo = GetComponent<PlayerData>(_player);
                    guiUpdateInfo.TouchedCell = hitEntity;
                    guiUpdateInfo.UpdateUI = true;
                    SetComponent(_player, guiUpdateInfo);
                }
            }
        }

        private bool Raycast(float3 rayStart, float3 rayEnd, out RaycastHit raycastHit)
        {
            var raycastInput = new RaycastInput
            {
                Start = rayStart,
                End = rayEnd,
                Filter = CollisionFilter.Default
            };
            return _collisionWorld.CastRay(raycastInput, out raycastHit);
        }
        
        
    }
}