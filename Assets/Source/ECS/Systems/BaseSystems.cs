using Source.ECS.Components;
using Source.ECS.Components.AuthoringComponents;
using Source.ECS.Utilities;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Assertions;

namespace Source.ECS.Systems
{
    public static class BaseSystems
    {
        //--------------------------------------------------------------

        public static Entity FindByName<TTagComponent>(FixedString128Bytes nameEntity, EntityManager entityManager)
        {
            var query = entityManager.CreateEntityQuery(ComponentType.ReadOnly<TTagComponent>(),
                ComponentType.ReadOnly<NameComponent>());
            if (query.IsEmpty) return Entity.Null;

            var entities = query.ToEntityArray(Allocator.Persistent);
            foreach (var entity in entities)
            {
                if (nameEntity == entityManager.GetComponentData<NameComponent>(entity).Value.ToString())
                {
                    var result = entity;
                    entities.Dispose();
                    return result;
                }
            }

            entities.Dispose();
            return Entity.Null;
        }

        public static Entity FindByCoordinate<TTagComponent>(int3 coordinate, EntityManager entityManager)
        {
            var query = entityManager.CreateEntityQuery(ComponentType.ReadOnly<TTagComponent>(),
                ComponentType.ReadOnly<CubeCoordinate>());
            if (query.IsEmpty) return Entity.Null;

            var entities = query.ToEntityArray(Allocator.Persistent);
            foreach (var entity in entities)
            {
                if (coordinate.Equals(entityManager.GetComponentData<CubeCoordinate>(entity).Value))
                {
                    var result = entity;
                    entities.Dispose();
                    return result;
                }
            }

            entities.Dispose();
            return Entity.Null;
        }

        public static Entity GetSingleEntity<TTagComponent>(EntityManager entityManager)
        {
            var query = entityManager.CreateEntityQuery(ComponentType.ReadOnly<TTagComponent>());
            var entities = query.ToEntityArray(Allocator.Persistent);
            var entity = entities[0];
            entities.Dispose();
            return entity;
        }

        public static Entity GetPrefabEntity<TTagComponent>(EntityManager entityManager)
        {
            var query = entityManager.CreateEntityQuery(
                ComponentType.ReadOnly<Prefab>(),
                ComponentType.ReadOnly<TTagComponent>());

            var entities = query.ToEntityArray(Allocator.Persistent);
            var entity = entities[0];
            entities.Dispose();
            return entity;
        }

        public static Entity GetPrefabEntity<TTagComponent>(FixedString128Bytes nameEntity, EntityManager entityManager)
        {
            var query = entityManager.CreateEntityQuery(ComponentType.ReadOnly<TTagComponent>(),
                ComponentType.ReadOnly<Prefab>(), ComponentType.ReadOnly<NameComponent>());
            if (query.IsEmpty) return Entity.Null;

            var entities = query.ToEntityArray(Allocator.Persistent);
            foreach (var entity in entities)
            {
                if (nameEntity == entityManager.GetComponentData<NameComponent>(entity).Value.ToString())
                {
                    var result = entity;
                    entities.Dispose();
                    return result;
                }
            }

            entities.Dispose();
            return Entity.Null;
        }

        //--------------------------------------------------------------

        public static int FindOrCreateInBuffer(Entity entity, DynamicBuffer<ResourceBuffer> buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                if (entity == buffer[i].Entity)
                    return i;
            }

            return buffer.Add(new ResourceBuffer { Entity = entity, Size = 0 });
        }
        
        public static int FindInBuffer(Entity entity, DynamicBuffer<ResourceBuffer> buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                if (entity == buffer[i].Entity)
                    return i;
            }

            return buffer.Add(new ResourceBuffer { Entity = entity, Size = 0 });
        }

        public static int FindInBuffer(Entity entity, DynamicBuffer<CellBuffer> buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                if (entity == buffer[i].Value)
                    return i;
            }

            return GameMath.IndexNone;
        }

        public static int FindInBuffer(Entity entity, DynamicBuffer<MarketResourceBuffer> buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                if (entity == buffer[i].ResourceEntity)
                    return i;
            }

            return GameMath.IndexNone;
        }

        public static int2 FindFreeInBuffer(Entity entity, DynamicBuffer<CellBuffer> cellBuffer, EntityManager entityManager)
        {
            for (int j = 0; j < cellBuffer.Length; j++)
            {
                var cellModifierBuffer = entityManager.GetBuffer<CellModifierBuffer>(cellBuffer[j].Value);
                
                for (int i = 0; i < cellModifierBuffer.Length; i++)
                {
                    if (cellModifierBuffer[i].Modifier == entity && !entityManager.Exists(cellModifierBuffer[i].Building))
                        return new int2(j, i);
                }
            }
            
            return new int2(GameMath.IndexNone);
        }
        
        //--------------------------------------------------------------

        public static void AddCell(Entity cellEntity, Entity regionEntity, EntityManager entityManager,
            bool forceAction = false)
        {
            Assert.IsTrue(entityManager.Exists(regionEntity));
            Assert.IsTrue(entityManager.Exists(cellEntity));

            if (forceAction)
            {
                var oldRegion = entityManager.GetComponentData<RegionRef>(cellEntity).Value;
                if (entityManager.Exists(oldRegion) && !entityManager.HasComponent<FractionalRegionTag>(oldRegion))
                {
                    var buffer = entityManager.GetBuffer<CellBuffer>(oldRegion);
                    var id = FindInBuffer(cellEntity, buffer);
                    Assert.IsTrue(FindInBuffer(cellEntity, buffer) != GameMath.IndexNone);
                    buffer.RemoveAt(id);
                }

                entityManager.SetComponentData(cellEntity, new RegionRef { Value = regionEntity });

                var cellBuffer = entityManager.GetBuffer<CellBuffer>(regionEntity);
                Assert.IsTrue(FindInBuffer(cellEntity, cellBuffer) == GameMath.IndexNone);
                cellBuffer.Add(new CellBuffer { Value = cellEntity });
            }
            else
            {
                var cellPrefab = GetPrefabEntity<CellTag>(entityManager);
                Assert.IsTrue(entityManager.Exists(cellPrefab));
                var buildData = entityManager.GetComponentData<BuildData>(cellPrefab);
                
                //Проверка на ресурсы
                {
                    var resourceBuffer = entityManager.GetBuffer<ResourceBuffer>(regionEntity);
                    var id = FindInBuffer(buildData.Resource, resourceBuffer);
                    if(id == GameMath.IndexNone) return;
                    if(resourceBuffer[id].Size < buildData.ResourceCount) return;
                }
               
                //Проверка на контроль
                var oldRegion = entityManager.GetComponentData<RegionRef>(cellEntity).Value;
                if (!entityManager.HasComponent<FractionalRegionTag>(oldRegion)) return;
                
                entityManager.SetComponentData(cellEntity, new RegionRef { Value = regionEntity });

                var cellBuffer = entityManager.GetBuffer<CellBuffer>(regionEntity);
                Assert.IsTrue(FindInBuffer(cellEntity, cellBuffer) == GameMath.IndexNone);
                cellBuffer.Add(new CellBuffer { Value = cellEntity });
                
                //Вычитаем ресурсы
                {
                    var resourceBuffer = entityManager.GetBuffer<ResourceBuffer>(regionEntity);
                    var id = FindInBuffer(buildData.Resource, resourceBuffer);
                    var resource = resourceBuffer[id];
                    resource.Size -= buildData.ResourceCount;
                    resourceBuffer[id] = resource;
                }
            }

            var query = entityManager.CreateEntityQuery(typeof(PlayerTag));
            var entities = query.ToEntityArray(Allocator.Persistent);
            for (var i = 0; i < entities.Length; i++)
            {
                var playerData = entityManager.GetComponentData<PlayerData>(entities[i]);
                playerData.UpdateMap = true;
                playerData.UpdateUI = true;
                entityManager.AddComponentData(entities[i], playerData);
            }

            entities.Dispose();
        }

        public static void AddBuilding(Entity regionEntity, FixedString128Bytes buildingName,
            EntityManager entityManager, bool forceAction = false)
        {
            Assert.IsTrue(entityManager.Exists(regionEntity));

            if (forceAction)
            {
                var buildingPrefab = GetPrefabEntity<BuildingTag>(buildingName, entityManager);
                Assert.IsTrue(entityManager.Exists(buildingPrefab));
                var buildingEntity = entityManager.Instantiate(buildingPrefab);
                entityManager.SetComponentData(buildingEntity, new Owner { Value = regionEntity });

                var buildingBuffer = entityManager.GetBuffer<BuildingBuffer>(regionEntity);
                buildingBuffer.Add(new BuildingBuffer { Value = buildingEntity });

                //Добавляем модификатор
                var buildData = entityManager.GetComponentData<BuildData>(buildingPrefab);
                if (buildData.NeedCellModifier)
                {
                    var modifierEntity = buildData.ModifierEntity;
                    var cellBuffer = entityManager.GetBuffer<CellBuffer>(regionEntity);
                    var cellModifierBuffer = entityManager.GetBuffer<CellModifierBuffer>(cellBuffer[0].Value);
                    var id = cellModifierBuffer.Add(new CellModifierBuffer
                    {
                        Modifier = modifierEntity,
                        Building = buildingEntity
                    });
                    entityManager.SetComponentData(buildingEntity, new ModifierRef
                    {
                        CellEntity = cellBuffer[0].Value,
                        Index = id
                    });
                }

                entityManager.RemoveComponent(buildingEntity, new ComponentTypes(EntitiesList.BuildBuildingTypes));
            }
            else
            {
                var buildingPrefab = GetPrefabEntity<BuildingTag>(buildingName, entityManager);
                Assert.IsTrue(entityManager.Exists(buildingPrefab));
                var buildData = entityManager.GetComponentData<BuildData>(buildingPrefab);
                
                //Проверка на модификатор
                int2 freeCellModifier = new int2();
                if (buildData.NeedCellModifier)
                {
                    var cellBuffer = entityManager.GetBuffer<CellBuffer>(regionEntity);
                    freeCellModifier = FindFreeInBuffer(buildData.ModifierEntity, cellBuffer, entityManager);
                    if(freeCellModifier.y == GameMath.IndexNone) return;
                }
                
                //Проверка на ресурсы
                {
                    var resourceBuffer = entityManager.GetBuffer<ResourceBuffer>(regionEntity);
                    var index = FindInBuffer(buildData.Resource, resourceBuffer);
                    if(index == GameMath.IndexNone) return;
                    if(resourceBuffer[index].Size < buildData.ResourceCount) return;
                }

                //Создаём здание
                var buildingEntity = entityManager.Instantiate(buildingPrefab);
                entityManager.SetComponentData(buildingEntity, new Owner { Value = regionEntity });
                var buildingBuffer = entityManager.GetBuffer<BuildingBuffer>(regionEntity);
                buildingBuffer.Add(new BuildingBuffer { Value = buildingEntity });
                
                //Добавляем модификатор
                if (buildData.NeedCellModifier)
                {
                    var cellBuffer = entityManager.GetBuffer<CellBuffer>(regionEntity);
                    var cellEntity = cellBuffer[freeCellModifier.x];
                    var cellModifierBuffer = entityManager.GetBuffer<CellModifierBuffer>(cellEntity.Value);
                    var modifier = cellModifierBuffer[freeCellModifier.y];
                    modifier.Building = buildingEntity;
                    cellModifierBuffer[freeCellModifier.y] = modifier;
                    entityManager.SetComponentData(buildingEntity, new ModifierRef
                    {
                        CellEntity = cellEntity.Value,
                        Index = freeCellModifier.y
                    });
                }
                
                //Вычитаем ресурсы
                {
                    var resourceBuffer = entityManager.GetBuffer<ResourceBuffer>(regionEntity);
                    var index = FindInBuffer(buildData.Resource, resourceBuffer);
                    var resource = resourceBuffer[index];
                    resource.Size -= buildData.ResourceCount;
                    resourceBuffer[index] = resource;
                }
            }
        }

        public static void TryBuy(FixedString128Bytes resourceName, int size, Entity countryEntity, EntityManager entityManager)
        {
            var resourceEntity = FindByName<ResourceTag>(resourceName, entityManager);
            var money = entityManager.GetComponentData<Money>(countryEntity);
            var capitalEntity = entityManager.GetComponentData<Capital>(countryEntity).Value;
            var resourceBuffer = entityManager.GetBuffer<ResourceBuffer>(capitalEntity);
               
            var regionEntity = entityManager.GetComponentData<RegionRef>(capitalEntity).Value;
            var marketEntity = entityManager.GetComponentData<MarketRef>(regionEntity).Value;
            var marketResourceBuffer = entityManager.GetBuffer<MarketResourceBuffer>(marketEntity);
            var indexInMarketResourceBuffer = FindInBuffer(resourceEntity, marketResourceBuffer);

            var newMoney = money.Value - size * marketResourceBuffer[indexInMarketResourceBuffer].Price;
            if (newMoney < 0) return;
            
            var marketResource = marketResourceBuffer[indexInMarketResourceBuffer];
            marketResource.Size -= size;
            if(marketResource.Size < 0) return;
            
            var index = FindOrCreateInBuffer(resourceEntity, resourceBuffer);
            var resource = resourceBuffer[index];
            resource.Size += size;
            
            entityManager.SetComponentData(countryEntity, new Money { Value = newMoney });
            marketResourceBuffer[indexInMarketResourceBuffer] = marketResource;
            resourceBuffer[index] = resource;
        }
        
        public static void TrySale(FixedString128Bytes resourceName, int size, Entity countryEntity, EntityManager entityManager)
        {
            var resourceEntity = FindByName<ResourceTag>(resourceName, entityManager);
            var money = entityManager.GetComponentData<Money>(countryEntity);
            var capitalEntity = entityManager.GetComponentData<Capital>(countryEntity).Value;
            var resourceBuffer = entityManager.GetBuffer<ResourceBuffer>(capitalEntity);
               
            var regionEntity = entityManager.GetComponentData<RegionRef>(capitalEntity).Value;
            var marketEntity = entityManager.GetComponentData<MarketRef>(regionEntity).Value;
            var marketResourceBuffer = entityManager.GetBuffer<MarketResourceBuffer>(marketEntity);
            var indexInMarketResourceBuffer = FindInBuffer(resourceEntity, marketResourceBuffer);

            var newMoney = money.Value + size * marketResourceBuffer[indexInMarketResourceBuffer].Price;
            
            var marketResource = marketResourceBuffer[indexInMarketResourceBuffer];
            marketResource.Size += size;
            
            var index = FindOrCreateInBuffer(resourceEntity, resourceBuffer);
            var resource = resourceBuffer[index];
            resource.Size -= size;
            if(resource.Size < 0) return;
            
            entityManager.SetComponentData(countryEntity, new Money { Value = newMoney });
            marketResourceBuffer[indexInMarketResourceBuffer] = marketResource;
            resourceBuffer[index] = resource;
        }
        
        //--------------------------------------------------------------

        /*
        public static bool CheckPossibilityAddExploringCellTask(Entity cell, Entity country,
            EntityManager entityManager)
        {
            var taskRulesEntity = GetSingleEntity<ExploringCellTaskRulesTag>(entityManager);
            var taskRules = entityManager.GetComponentData<ExploringCellData>(taskRulesEntity);

            //Не исследована ли уже клетка
            {
                if (CheckCountryCellExplored(cell, country, entityManager))
                    return false;
            }

            //Провека на дубликацию задания
            {
                var taskQuery = entityManager.CreateEntityQuery(ComponentType.ReadOnly<ExploringCellTaskTag>());
                var entities = taskQuery.ToEntityArray(Allocator.Persistent);
                foreach (var task in entities)
                {
                    if (cell == entityManager.GetComponentData<CellRef>(task).Value)
                        if (country == entityManager.GetComponentData<CountryRef>(task).Value)
                        {
                            entities.Dispose();
                            return false;
                        }
                }

                entities.Dispose();
            }

            //Проверка на ресурсы
            {
                var mainCity = entityManager.GetComponentData<Capital>(country).Value;
                var resources = entityManager.GetBuffer<ResourceBuffer>(mainCity);
                var resourcesId = FindOrCreateInBuffer(taskRules.Resource, resources);
                if (resources[resourcesId].Size < taskRules.ResourceCount) return false;
            }

            //Проверка на исследованных соседей
            {
                var check = false;
                var neighbors = entityManager.GetBuffer<CellNeighbors>(cell);
                foreach (var neighbor in neighbors)
                {
                    if (CheckCountryCellExplored(neighbor.Value, country, entityManager))
                    {
                        check = true;
                        break;
                    }
                }

                if (!check) return false;
            }

            return true;
        }

        public static void AddExploringCellTask(Entity cell, Entity country, EntityManager entityManager)
        {
            var taskRulesEntity = GetSingleEntity<ExploringCellTaskRulesTag>(entityManager);
            var taskRules = entityManager.GetComponentData<ExploringCellData>(taskRulesEntity);
            var prefab = GetPrefabEntity<ExploringCellTaskTag>(entityManager);

            //Вычитание ресурса
            {
                var mainCity = entityManager.GetBuffer<CitiesRef>(country)[0].Value;
                var resources = entityManager.GetBuffer<ResourceBuffer>(mainCity);
                var resourcesId = FindOrCreateInBuffer(taskRules.Resource, resources);
                var tempResource = resources[resourcesId];
                tempResource.Size -= taskRules.ResourceCount;
                resources[resourcesId] = tempResource;
            }

            //Создание задания
            {
                var task = entityManager.Instantiate(prefab);
                entityManager.SetComponentData(task, new TurnCounter { Value = 0 });
                entityManager.SetComponentData(task, new CountryRef { Value = country });
                entityManager.SetComponentData(task, new CellRef { Value = cell });
            }
        }

        public static bool CheckPossibilityAddBuildBuildingTask(Entity city, Entity building, Entity country,
            EntityManager entityManager)
        {
            var buildRules = entityManager.GetComponentData<BuildData>(building);

            //Проверка на пренадлежность города стране
            {
                var check = false;
                var countryCities = entityManager.GetBuffer<CitiesRef>(country);
                foreach (var countryCity in countryCities)
                {
                    if (city == countryCity.Value)
                    {
                        check = true;
                        break;
                    }
                }

                if (!check) return false;
            }

            //Проверка на лимит по зданиям
            {
                var cityRuleEntity = GetSingleEntity<CityRulesTag>(entityManager);
                var cityRule = entityManager.GetComponentData<CityRulesData>(cityRuleEntity);

                var buildings = entityManager.GetBuffer<BuildingBuffer>(city);
                var cityType = entityManager.GetComponentData<CitySize>(city);
                if (buildings.Length >= cityRule.BuildingCount[(int)cityType.Value])
                    return false;
            }

            //Провека на строительство в городе
            {
                var taskQuery = entityManager.CreateEntityQuery(ComponentType.ReadOnly<BuildBuildingTaskTag>());
                var entities = taskQuery.ToEntityArray(Allocator.Persistent);
                foreach (var task in entities)
                {
                    if (city == entityManager.GetComponentData<CityRef>(task).Value)
                    {
                        entities.Dispose();
                        return false;
                    }
                }

                entities.Dispose();
            }

            //Проверка на ресурсы
            {
                var resources = entityManager.GetBuffer<ResourceBuffer>(city);
                var resourcesId = FindOrCreateInBuffer(buildRules.Resource, resources);
                if (resources[resourcesId].Size < buildRules.ResourceCount) return false;
            }

            return true;
        }
        
        public static void AddBuilding(Entity city, Entity building, EntityManager entityManager, bool forceAction = false)
        {
            var buildRules = entityManager.GetComponentData<BuildData>(building);
            var prefab = GetPrefabEntity<BuildBuildingTaskTag>(entityManager);

            //Вычитание ресурса
            {
                var resources = entityManager.GetBuffer<ResourceBuffer>(city);
                var resourcesId = FindOrCreateInBuffer(buildRules.Resource, resources);
                var tempResource = resources[resourcesId];
                tempResource.Size -= buildRules.ResourceCount;
                resources[resourcesId] = tempResource;
            }

            //Создание задания
            {
                var task = entityManager.Instantiate(prefab);
                entityManager.SetComponentData(task, new TurnCounter { Value = 0 });
                entityManager.SetComponentData(task, new CityRef { Value = city });
                entityManager.SetComponentData(task, new BuildingRef { Value = building });
            }
        }

        
        */
    }
}