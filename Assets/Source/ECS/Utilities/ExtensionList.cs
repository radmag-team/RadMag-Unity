using Unity.Mathematics;
using UnityEngine;

namespace Source.ECS.Utilities
{
    public static class ExtensionList {
        public static HexDirection Opposite (this HexDirection direction) {
            return (int)direction < 3 ? (direction + 3) : (direction - 3);
        }
        
        public static float4 CovertToFloat4(this Color color)
        {
            return new float4(color.r, color.g, color.b, color.a);
        }
    }
}