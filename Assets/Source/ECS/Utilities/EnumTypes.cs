namespace Source.ECS.Utilities
{
    public enum MapType
    {
        PoliticalMap
    }

    public enum HexDirection
    {
        NE,
        E,
        SE,
        SW,
        W,
        NW
    }
    

    /*
    public enum ResourceLocation
    {
        Cell = 0,
        City  = 1,
    }
    
    
    public enum CitySizeEnum
    {
        Small = 0,
        Medium = 1,
        Large = 2,
    }
    */
}