using Unity.Mathematics;

namespace Source.ECS.Utilities
{
    public static class GameMath
    {
        public static readonly int IndexNone = -1;
        
        private static readonly float outerRadius = 1f;
        private static readonly float outerToInner = 0.866025404f;
        private static readonly float InnerRadius = outerRadius * outerToInner;
        
        public static int3 ConvertToCubeCoordinate(int2 offsetCoordinate)
        {
            var x = offsetCoordinate.x - (offsetCoordinate.y - (offsetCoordinate.y & 1)) / 2;
            var z = offsetCoordinate.y;
            var y = -x - z;
            return new int3(x, y, z);
        }

        public static int2 ConvertToOffsetCoordinate(int3 cubeCoordinate)
        {
            var col = cubeCoordinate.x + (cubeCoordinate.z - (cubeCoordinate.z & 1)) / 2;
            var row = cubeCoordinate.z;
            return new int2(col, row);
        }
        
        public static float3 ConvertToRealCoordinate(int2 offsetCoordinate)
        {
            // ReSharper disable once PossibleLossOfFraction
            var x = (offsetCoordinate.x + offsetCoordinate.y * 0.5f - offsetCoordinate.y / 2) *
                    InnerRadius * 2;
            var z = 0f;
            var y = offsetCoordinate.y * (outerRadius * 1.5f);
            return new float3(x, y, z);
        }
    }
}