using Source.ECS.Components;
using Source.ECS.Components.AuthoringComponents;
using Unity.Entities;

namespace Source.ECS.Utilities
{
    public static class EntitiesList
    {
        public static readonly ComponentType[] GameModeTypes =
        {
            typeof(GameModeTag),
            typeof(TurnCounter),
            typeof(IsNextTurn),
        };

        public static readonly ComponentType[] PlayerTypes =
        {
            typeof(PlayerTag),
            typeof(PlayerData),
        };
        
        //--------------------------------------------------------------

        public static readonly ComponentType[] CellPrefabTypes =
        {
            typeof(Prefab),
            typeof(LinkedEntityGroup),
            typeof(CellTag),
            typeof(CubeCoordinate),
            typeof(RegionRef),
            typeof(CellNeighbors),
            typeof(ColorComponent),
            typeof(CellModifierBuffer),
            typeof(BuildData),
        };

        public static readonly ComponentType[] RegionPrefabTypes =
        {
            typeof(Prefab),
            typeof(LinkedEntityGroup),
            typeof(RegionTag),
            typeof(NameComponent),
            typeof(CellBuffer),
            typeof(ResourceBuffer),
            typeof(BuildingBuffer),
            typeof(Owner),
            typeof(MarketRef)
        };
        
        public static readonly ComponentType[] ResourcePrefabTypes =
        {
            typeof(Prefab),
            typeof(LinkedEntityGroup),
            typeof(ResourceTag),
            typeof(NameComponent),
        };
        
        public static readonly ComponentType[] CellModifierPrefabTypes =
        {
            typeof(Prefab),
            typeof(LinkedEntityGroup),
            typeof(CellModifierTag),
            typeof(NameComponent),
        };
      
        public static readonly ComponentType[] BuildingPrefabTypes = 
        {
            typeof(Prefab),
            typeof(LinkedEntityGroup),
            typeof(BuildingTag),
            typeof(NameComponent),
            typeof(InputBuffer),
            typeof(OutputBuffer),
            typeof(Owner),
            typeof(ModifierRef),
            typeof(BuildData),
        };

        public static readonly ComponentType[] CountryPrefabTypes =
        {
            typeof(Prefab),
            typeof(LinkedEntityGroup),
            typeof(CountryTag),
            typeof(NameComponent),
            typeof(Capital),
            typeof(Owner),
            typeof(ColorComponent),
            typeof(Money)
        };
        
        public static readonly ComponentType[] MarketPrefabTypes =
        {
            typeof(Prefab),
            typeof(LinkedEntityGroup),
            typeof(MarketTag),
            typeof(NameComponent),
            typeof(MarketResourceBuffer),
            typeof(Owner),
        };
        
        //--------------------------------------------------------------

        public static readonly ComponentType[] SubRegionTypes =
        {
            typeof(SubRegionTag),
            typeof(RegionRef),
        };
        
        public static readonly ComponentType[] FractionalRegionTypes =
        {
            typeof(FractionalRegionTag),
            typeof(RegionBuffer),
        };
        
        public static readonly ComponentType[] BuildBuildingTypes =
        {
            typeof(WorkInProgressTag),
            typeof(TurnCounter),
        };
        
        //--------------------------------------------------------------
        
        /*
        
        public static readonly ComponentType[] ExploringCellTaskPrefabTypes =
        {
            ComponentType.ReadOnly<Prefab>(),
            typeof(LinkedEntityGroup),
            ComponentType.ReadOnly<ExploringCellTaskTag>(),
            typeof(TurnCounter),
            typeof(CountryRef),
            typeof(CellRef),
            typeof(ExploringCellData),
        };
         */
    }
}