using System;
using Source.ECS.Components;
using Source.ECS.Components.AuthoringComponents;
using Unity.Collections;
using Unity.Entities;
using Source.ECS.Systems;
using Source.ECS.Utilities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Assertions;
using Random = Unity.Mathematics.Random;


namespace Source.InitMode
{
    public class InitEditorLevel : MonoBehaviour
    {
        [SerializeField] public int mapLength;
        [SerializeField] public int mapWidth;
        [SerializeField] public GameObject cellPrefab;

        private EntityManager _entityManager;
        private World _world;
        private BlobAssetStore _blobAssetStore;
        private GameObjectConversionSettings _settings;
        private Random _random;


        void Start()
        {
            _world = World.DefaultGameObjectInjectionWorld;
            _entityManager = _world.EntityManager;
            _blobAssetStore = new BlobAssetStore();
            _settings = GameObjectConversionSettings.FromWorld(_entityManager.World, _blobAssetStore);
            _random = new Random(1);

            CreatePrefabs();
            CreateGameModes();
            InitPlayer();
            CreateResources();
            CreateCellPrefab();
            CreateCellModifiers();
            CreateBuildingPrefabs();
            
            var cellEntities = CreateEmptyMap();
            AddCellModifiers(cellEntities);
            var mainRegionEntity = CreateMainRegion(cellEntities, ImportList.RegionInitData);
            CreateCountry(ImportList.CountryInitData, mainRegionEntity,
                BaseSystems.GetSingleEntity<PlayerTag>(_entityManager));

            cellEntities.Dispose();

            var nextTurnSystem = _world.CreateSystem<NextTurnSystem>();
            _world.GetExistingSystem<SimulationSystemGroup>().AddSystemToUpdateList(nextTurnSystem);

            var inputSystem = _world.CreateSystem<InputSystem>();
            _world.GetExistingSystem<SimulationSystemGroup>().AddSystemToUpdateList(inputSystem);

            var uiSystem = _world.CreateSystem<UISystem>();
            _world.GetExistingSystem<SimulationSystemGroup>().AddSystemToUpdateList(uiSystem);

            var mapSystem = _world.CreateSystem<MapSystem>();
            _world.GetExistingSystem<SimulationSystemGroup>().AddSystemToUpdateList(mapSystem);
        }

        public void OnDestroy()
        {
            _blobAssetStore.Dispose();
        }

        private void CreatePrefabs()
        {
            {
                var entity = _entityManager.CreateEntity(EntitiesList.RegionPrefabTypes);
                var buffer = _entityManager.GetBuffer<LinkedEntityGroup>(entity);
                buffer.Add(entity);
            }
            {
                var entity = _entityManager.CreateEntity(EntitiesList.CountryPrefabTypes);
                var buffer = _entityManager.GetBuffer<LinkedEntityGroup>(entity);
                buffer.Add(entity);
            }
            {
                var entity = _entityManager.CreateEntity(EntitiesList.ResourcePrefabTypes);
                var buffer = _entityManager.GetBuffer<LinkedEntityGroup>(entity);
                buffer.Add(entity);
            }
            {
                var entity = _entityManager.CreateEntity(EntitiesList.ResourcePrefabTypes);
                var buffer = _entityManager.GetBuffer<LinkedEntityGroup>(entity);
                buffer.Add(entity);
            }
            {
                var entity = _entityManager.CreateEntity(EntitiesList.CellModifierPrefabTypes);
                var buffer = _entityManager.GetBuffer<LinkedEntityGroup>(entity);
                buffer.Add(entity);
            }
            {
                var entity = _entityManager.CreateEntity(EntitiesList.MarketPrefabTypes);
                var buffer = _entityManager.GetBuffer<LinkedEntityGroup>(entity);
                buffer.Add(entity);
            }
        }

        private void CreateGameModes()
        {
            _entityManager.CreateEntity(EntitiesList.GameModeTypes);
        }

        private void InitPlayer()
        {
            var player = BaseSystems.GetSingleEntity<PlayerTag>(_entityManager);
            _entityManager.AddComponents(player, new ComponentTypes(EntitiesList.PlayerTypes));

            var playerData = _entityManager.GetComponentData<PlayerData>(player);
            playerData.CurrentMap = MapType.PoliticalMap;
            playerData.UpdateMap = true;
            _entityManager.SetComponentData(player, playerData);
        }

        private void CreateResources()
        {
            foreach (var initData in ImportList.ResourceInitDataArray)
            {
                var resourcePrefab = BaseSystems.GetPrefabEntity<ResourceTag>(_entityManager);
                var resourceEntity = _entityManager.Instantiate(resourcePrefab);
                _entityManager.SetComponentData(resourceEntity, new NameComponent { Value = initData.Name });
            }
        }

        private void CreateCellPrefab()
        {
            var entity = GameObjectConversionUtility.ConvertGameObjectHierarchy(cellPrefab, _settings);
            _entityManager.AddComponents(entity, new ComponentTypes(EntitiesList.CellPrefabTypes));
            _entityManager.AddComponentData(entity,
                new BuildData
                {
                    Resource = BaseSystems
                        .FindByName<ResourceTag>(ImportList.ResourceInitDataArray[0].Name, _entityManager),
                    ResourceCount = 1,
                });
        }

        private void CreateCellModifiers()
        {
            foreach (var initData in ImportList.CellModifierInitDataArray)
            {
                var prefab = BaseSystems.GetPrefabEntity<CellModifierTag>(_entityManager);
                var entity = _entityManager.Instantiate(prefab);
                _entityManager.SetComponentData(entity, new NameComponent { Value = initData.Name });
            }
        }

        private void CreateBuildingPrefabs()
        {
            var buildingInitDataArray = ImportList.BuildingInitDataArray;
            foreach (var initData in buildingInitDataArray)
            {
                var buildingEntity = _entityManager.CreateEntity(EntitiesList.BuildingPrefabTypes);
                _entityManager.AddComponents(buildingEntity, new ComponentTypes(EntitiesList.BuildBuildingTypes));

                var buffer = _entityManager.GetBuffer<LinkedEntityGroup>(buildingEntity);
                buffer.Add(buildingEntity);

                _entityManager.SetComponentData(buildingEntity, new NameComponent { Value = initData.Name });

                var inputBuffer = _entityManager.GetBuffer<InputBuffer>(buildingEntity);
                foreach (var (resName, size) in initData.Input)
                {
                    var resourceEntity = BaseSystems.FindByName<ResourceTag>(resName, _entityManager);
                    Assert.IsTrue(_entityManager.Exists(resourceEntity));
                    inputBuffer.Add(new InputBuffer
                    {
                        Entity = resourceEntity,
                        Size = size,
                    });
                }

                var outputBuffer = _entityManager.GetBuffer<OutputBuffer>(buildingEntity);
                foreach (var (resName, size) in initData.Output)
                {
                    var resourceEntity = BaseSystems.FindByName<ResourceTag>(resName, _entityManager);
                    Assert.IsTrue(_entityManager.Exists(resourceEntity));
                    outputBuffer.Add(new OutputBuffer
                    {
                        Entity = resourceEntity,
                        Size = size,
                    });
                }

                _entityManager.SetComponentData(buildingEntity, new BuildData
                {
                    ResourceCount = 2,
                    Resource = BaseSystems.FindByName<ResourceTag>("Supplies", _entityManager),
                    Time = 1,
                    NeedCellModifier = initData.NeedCellModifier,
                    ModifierEntity = BaseSystems.FindByName<CellModifierTag>(initData.ModifierName, _entityManager)
                });
            }
        }

        private NativeArray<Entity> CreateEmptyMap()
        {
            var cellEntityPrefab = BaseSystems.GetPrefabEntity<CellTag>(_entityManager);
            var cells = new NativeArray<Entity>(mapWidth * mapLength, Allocator.Persistent);
            _entityManager.Instantiate(cellEntityPrefab, cells);

            var neighborsInitArray = new NativeArray<CellNeighbors>(6, Allocator.Persistent);

            var setNeighbor = new Action<int, int, HexDirection>((index, neighborIndex, direction) =>
            {
                var cellEntity = cells[index];
                var cellNeighbors = _entityManager.GetBuffer<CellNeighbors>(cellEntity);

                var neighborEntity = cells[neighborIndex];
                var neighborNeighbors = _entityManager.GetBuffer<CellNeighbors>(neighborEntity);

                cellNeighbors[(int)direction] = new CellNeighbors { Value = neighborEntity };
                neighborNeighbors[(int)direction.Opposite()] = new CellNeighbors { Value = cellEntity };
            });

            for (int z = 0; z < mapWidth; z++)
            {
                for (int x = 0; x < mapLength; x++)
                {
                    var index = x + z * mapLength;
                    var cell = cells[index];

                    _entityManager.SetComponentData(cell,
                        new CubeCoordinate { Value = GameMath.ConvertToCubeCoordinate(new int2(x, z)) });
                    _entityManager.SetComponentData(cell,
                        new Translation { Value = GameMath.ConvertToRealCoordinate(new int2(x, z)) });

                    var neighbors = _entityManager.GetBuffer<CellNeighbors>(cell);
                    neighbors.AddRange(neighborsInitArray);

                    if (x > 0)
                    {
                        setNeighbor(index, index - 1, HexDirection.W);
                    }

                    if (z > 0)
                    {
                        if ((z & 1) == 0)
                        {
                            setNeighbor(index, index - mapLength, HexDirection.SE);
                            if (x > 0) setNeighbor(index, index - mapLength - 1, HexDirection.SW);
                        }
                        else
                        {
                            setNeighbor(index, index - mapLength, HexDirection.SW);
                            if (x < mapLength - 1) setNeighbor(index, index - mapLength + 1, HexDirection.SE);
                        }
                    }
                }
            }

            neighborsInitArray.Dispose();

            return cells;
        }

        private void AddCellModifiers(NativeArray<Entity> cellEntities)
        {
            var query = _entityManager.CreateEntityQuery(ComponentType.ReadOnly<CellModifierTag>());
            var cellModifiers = query.ToEntityArray(Allocator.Persistent);
            for (var i = 0; i < cellEntities.Length; i++)
            {
                var cellModifierBuffer = _entityManager.GetBuffer<CellModifierBuffer>(cellEntities[i]);
                cellModifierBuffer.Add(new CellModifierBuffer
                {
                    Modifier = cellModifiers[_random.NextInt(0, cellModifiers.Length)]
                });
            }

            cellModifiers.Dispose();
        }

        private Entity CreateMainRegion(NativeArray<Entity> cellEntities, RegionInitData regionInitData)
        {
            var regionPrefab = BaseSystems.GetPrefabEntity<RegionTag>(_entityManager);
            var regionEntity = _entityManager.Instantiate(regionPrefab);
            _entityManager.AddComponents(regionEntity, new ComponentTypes(EntitiesList.FractionalRegionTypes));

            _entityManager.AddComponentData(regionEntity, new NameComponent { Value = regionInitData.Name });
            // ReSharper disable once ForCanBeConvertedToForeach
            for (int i = 0; i < cellEntities.Length; i++)
            {
                BaseSystems.AddCell(cellEntities[i], regionEntity, _entityManager, true);
            }

            CreateRegionMarket(regionEntity);

            return regionEntity;
        }

        private Entity CreateRegionMarket(Entity region)
        {
            var regionMarketPrefab = BaseSystems.GetPrefabEntity<MarketTag>(_entityManager);
            var regionMarketEntity = _entityManager.Instantiate(regionMarketPrefab);

            _entityManager.AddComponentData(region, new MarketRef { Value = regionMarketEntity });

            {
                _entityManager.AddComponentData(regionMarketEntity, new NameComponent { Value = "TestMarket" });
                _entityManager.AddComponentData(regionMarketEntity, new Owner { Value = regionMarketEntity });

                var marketResourceBuffers = _entityManager.GetBuffer<MarketResourceBuffer>(regionMarketEntity);
                foreach (var initData in ImportList.ResourceInitDataArray)
                {
                    marketResourceBuffers.Add(new MarketResourceBuffer
                    {
                        ResourceEntity = BaseSystems.FindByName<ResourceTag>(initData.Name, _entityManager),
                        Size = 1000,
                        Price = 10,
                    });
                }
            }

            return regionMarketEntity;
        }

        private void CreateCountry(CountryInitData countryInitData, Entity mainRegion, Entity playerEntity)
        {
            Entity regionEntity;
            {
                var regionPrefab = BaseSystems.GetPrefabEntity<RegionTag>(_entityManager);
                regionEntity = _entityManager.Instantiate(regionPrefab);
                _entityManager.AddComponents(regionEntity, new ComponentTypes(EntitiesList.SubRegionTypes));

                _entityManager.AddComponentData(regionEntity, new NameComponent { Value = countryInitData.RegionName });
                foreach (var coordinate in countryInitData.CellsCoords)
                {
                    BaseSystems.AddCell(BaseSystems.FindByCoordinate<CellTag>(coordinate, _entityManager), regionEntity,
                        _entityManager, true);
                }

                foreach (var buildingName in countryInitData.BuildingNames)
                {
                    BaseSystems.AddBuilding(regionEntity, buildingName, _entityManager, true);
                }

                _entityManager.AddComponentData(regionEntity, new RegionRef { Value = mainRegion });
            }

            Entity countryEntity;
            {
                var countryPrefab = BaseSystems.GetPrefabEntity<CountryTag>(_entityManager);
                countryEntity = _entityManager.Instantiate(countryPrefab);
                _entityManager.AddComponentData(countryEntity, new NameComponent { Value = countryInitData.Name });
                _entityManager.AddComponentData(countryEntity, new Capital { Value = regionEntity });
                _entityManager.AddComponentData(countryEntity, new Owner { Value = playerEntity });
                _entityManager.AddComponentData(countryEntity,
                    new ColorComponent { Value = countryInitData.CountryColor.CovertToFloat4() });
                _entityManager.AddComponentData(countryEntity, new Money { Value = 100 });
            }

            {
                _entityManager.AddComponentData(regionEntity, new Owner { Value = countryEntity });
            }

            {
                var playerData = _entityManager.GetComponentData<PlayerData>(playerEntity);
                playerData.OwnCountry = countryEntity;
                _entityManager.SetComponentData(playerEntity, playerData);
            }
        }
    }
}