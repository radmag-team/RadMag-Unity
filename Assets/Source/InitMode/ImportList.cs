using System;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Source.InitMode
{
    public struct ResourceInitData
    {
        public FixedString128Bytes Name;
    }

    public struct CellModifierInitData
    {
        public FixedString128Bytes Name;
    }
    
    public struct BuildingInitData
    {
        public FixedString128Bytes Name;
        public (string, float)[] Input;
        public (string, float)[] Output;
        public bool NeedCellModifier;
        public FixedString128Bytes ModifierName;
    };

    public struct RegionInitData
    {
        public FixedString128Bytes Name;
    }

    public struct CountryInitData
    {
        public FixedString128Bytes Name;
        public Color CountryColor;
        public FixedString128Bytes RegionName;
        public int3[] CellsCoords;
        public FixedString128Bytes[] BuildingNames;
    };
    
    public static class ImportList
    {
        public static readonly ResourceInitData[] ResourceInitDataArray =
        {
            new ResourceInitData { Name = "Food"},
            new ResourceInitData { Name = "Medication"},
            new ResourceInitData { Name = "Clothing"},
            new ResourceInitData { Name = "Luxury"},
            new ResourceInitData { Name = "Tool"},
            new ResourceInitData { Name = "Supplies"},
            new ResourceInitData { Name = "Fuel"},
            new ResourceInitData { Name = "Weapon"},
        };

        public static readonly CellModifierInitData[] CellModifierInitDataArray =
        {
            new CellModifierInitData { Name = "Pure land"},
            new CellModifierInitData { Name = "Dump"},
        };
        
        public static readonly BuildingInitData[] BuildingInitDataArray =
        {
            new BuildingInitData
            {
                Name = "Farm",
                Input = Array.Empty<(string, float)>(),
                Output = new (string, float)[]
                {
                   ("Food", 1)
                },
                NeedCellModifier = true,
                ModifierName = "Pure land"
            },
            new BuildingInitData
            {
                Name = "Pharmacy",
                Input = Array.Empty<(string, float)>(),
                Output = new (string, float)[]
                {
                    ("Medication", 1)
                },
                NeedCellModifier = true,
                ModifierName = "Pure land"
            },
            new BuildingInitData
            {
                Name = "Cotton farms",
                Input = Array.Empty<(string, float)>(),
                Output = new (string, float)[]
                {
                    ("Clothing", 1)
                },
                NeedCellModifier = true,
                ModifierName = "Pure land"
            },
            new BuildingInitData
            {
                Name = "Winery",
                Input = Array.Empty<(string, float)>(),
                Output = new (string, float)[]
                {
                    ("Luxury", 1)
                },
                NeedCellModifier = true,
                ModifierName = "Pure land"
            },
            new BuildingInitData
            {
                Name = "Workshop",
                Input = Array.Empty<(string, float)>(),
                Output = new (string, float)[]
                {
                    ("Tool", 1)
                },
                NeedCellModifier = true,
                ModifierName = "Dump"
            },
            new BuildingInitData
            {
                Name = "Brickyard",
                Input = Array.Empty<(string, float)>(),
                Output = new (string, float)[]
                {
                    ("Supplies", 1)
                },
                NeedCellModifier = true,
                ModifierName = "Dump"
            },
            new BuildingInitData
            {
                Name = "Oil Factory",
                Input = Array.Empty<(string, float)>(),
                Output = new (string, float)[]
                {
                    ("Fuel", 1)
                },
                NeedCellModifier = true,
                ModifierName = "Dump"
            },
            new BuildingInitData
            {
                Name = "Arms Factory",
                Input = Array.Empty<(string, float)>(),
                Output = new (string, float)[]
                {
                    ("Weapon", 1)
                },
                NeedCellModifier = true,
                ModifierName = "Dump"
            },
        };

        public static readonly RegionInitData RegionInitData = new RegionInitData
        {
            Name = "BigTestRegion"
        };
        
        public static readonly CountryInitData CountryInitData = new CountryInitData
        {
            Name = "CyanCountry",
            RegionName = "CyanRegion",
            CountryColor = Color.cyan,
            CellsCoords = new [] { int3.zero },
            BuildingNames = new FixedString128Bytes[] { "Farm" },
        };
        
        
    }
}